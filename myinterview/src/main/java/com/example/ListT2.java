package com.example;

public class ListT2 {
    private Node inicial;
    private Node last;
    private int size;

    public Node getInicial() {
        return inicial;
    }

    public void setInicial(Node inicial) {
        this.inicial = inicial;
    }

    public Node getLast() {
        return last;
    }

    public void setLast(Node last) {
        this.last = last;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void add(String value) {

        Node node = new Node(value);

        if(this.inicial == null && this.last == null) {
            this.inicial = node;
            this.last = node;
        } else {
            this.last.setNext(node);
            this.last = node;
        }

        this.size++;
    }

    public void remove(int index) {
        try{
            Node prev = getNode(index - 1);
            Node delete = prev.getNext();
            prev.setNext(delete.getNext());
            delete = null; 
            this.size--;
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public Node getNode(int index) {
        Node node;

        if(index < 1 || this.inicial == null) return null;
        else {
            node = this.inicial;
            while(index > 1) {
                node = node.getNext();
                if(node == null) return null;
                index--;
            }

            return node;

        }
    }

}
