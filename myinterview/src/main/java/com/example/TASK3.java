package com.example;

import java.util.ArrayList;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();

        int num = (int)(Math.random() * 10);
        System.out.println(num);

        for(int i = 0; i < num; i++){
            list.add(String.valueOf((int)(Math.random() * 10)));
        }        
        
        System.out.println("Quantidade de itens distintos existentes: " + countDistinctItens(list));

    }

    private static Integer countDistinctItens(ArrayList<String> list){
        ArrayList<String> index = new ArrayList<String>();

        for (String str : list) {
            if(!index.contains(str)){
               index.add(str);
            }
        } 
        
        return index.size();
    }
}
