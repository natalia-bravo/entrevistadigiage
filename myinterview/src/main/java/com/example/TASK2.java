package com.example;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 */


public class TASK2 {

    public static void main(String[] args) {

       ListT2 list = new ListT2();

       writeList(list);

       System.out.println(list.getSize());

       list.remove(list.getSize()/2);

       System.out.println(list.getSize());
    }

    private static void writeList (ListT2 list){
        for (int i = 1; i <= 15; i++) list.add(Integer.toString(i));
    }
}